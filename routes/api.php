<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TodoController;

Route::middleware(['auth:sanctum'])->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('todo_list', [TodoController::class,"getAllTodoList"]);
Route::post('todo_list', [TodoController::class,"addTodoList"]);
Route::put('todo_list/update/{id}', [TodoController::class,"editTodoList"]);
Route::put('todo_list/delete/{id}', [TodoController::class,"deleteTodoList"]);
