<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Todo;
use Validator;
class TodoController extends Controller
{
    public function getAllTodoList(){
        $todo = Todo::all();
        $data = [
            'status'=>200,
            'todo'=>$todo
        ];
        return response()->json($data,200);
    }

    public function addTodoList(Request $request){
        $validator = Validator::make($request->all(),[
            'task'=>'required',
            'status'=> 'required'
        ]);
        if($validator->fails()){
            $data=[
                'status'=> 422,
                'messages'=>"Task and status fields are required."
            ];
            return response()->json($data,422);
        }else{
            $todo = new Todo();
            $todo->task = $request->task;
            $todo->status = $request->status;
            $todo->save();
            $data=[
                'status'=> 200,
                'messages'=>"Added new data."
            ];
            return response()->json($data,200);
        }
    }
    public function editTodoList(Request $request, $id){
        $validator = Validator::make($request->all(),[
            'task'=>'required',
            'status'=> 'required'
        ]);
        if($validator->fails()){
            $data=[
                'status'=> 422,
                'messages'=>"Task and status fields are required."
            ];
            return response()->json($data,422);
        }else{
            $todo =Todo::find( $id );
            $todo->task = $request->task;
            $todo->status = $request->status;
            $todo->save();
            $data=[
                'status'=> 200,
                'messages'=>"Data Updated."
            ];
            return response()->json($data,200);
        }
    }
    
    public function deleteTodoList(Request $request, $id){
        $todo =Todo::find( $id );
        $todo ->delete();
        $data=[
            'status'=> 200,
            'messages'=>"Data deleted."
        ];
        return response()->json($data,200);
    }
}
